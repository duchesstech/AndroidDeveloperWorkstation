# Android Developer Workstation

Automation of Android Developer Workstation

![vagrant-virtualbox-ubuntu](/resources/vagrant-virtualbox-ubuntu-post-title.png)

## Installation ##
1. Download/Install Vagrant 1.8.5 ([Windows](https://releases.hashicorp.com/vagrant/1.8.5/vagrant_1.8.5.msi), [Mac OS X](https://releases.hashicorp.com/vagrant/1.8.5/vagrant_1.8.5.dmg)).

2. Download/Install VirtualBox 5.1.6 ([Windows](http://download.virtualbox.org/virtualbox/5.1.6/VirtualBox-5.1.6-110634-Win.exe), [Mac OS X](http://download.virtualbox.org/virtualbox/5.1.6/VirtualBox-5.1.6-110634-OSX.dmg)).

3. Download VirtualBox Extension Pack 5.1.6 ([All Platforms](http://download.virtualbox.org/virtualbox/5.1.6/Oracle_VM_VirtualBox_Extension_Pack-5.1.6-110634.vbox-extpack)).

    a. Run VirtualBox as administrator.
    
    ![VirtualBox Admin Rights](/resources/vbAdminRights.JPG?raw=true)
    
    b. Go to preferences.
    
    ![VirtualBox Preferences](/resources/vbPreferences.JPG?raw=true)
    
    c. Go to extensions and add a new package. Select the extension pack that was just downloaded and follow the installation.
    
    ![VirtualBox Add Package](/resources/vbAddPackage.JPG?raw=true)
    
4. Clone this repository. Modify "vagrant_properties.yml" to fill your tooling needs. Copy over your private GitHub SSH key into the /shared directory and rename it "github_rsa" if you want to reuse your host machines GitHub SSH Key. You can always configure your Git from within the guest machine if you'd rather do that.

5. Open a terminal at this repository's directory. Run command `vagrant plugin install vagrant-vbguest`. This will automatically keep VirtualBox guest additions up to date.

6. If on Windows, ensure your command prompt does not have QuickEdit mode on (right-click top of window -> Properties -> Options -> Edit Options -> Uncheck QuickEdit Mode -> OK). Run command `vagrant up`. Sit back and wait for the scripts to complete. The time of this entirely depends on your network speeds. With decent speeds, it would take ~30min.

7. Log into your VirtualBox machine with credentials user: vagrant, password: vagrant. Start the desktop with command `startxfce4&`. Use the default config for the panel when prompted.

![XFCE Panel Start](/resources/xfcePanel.JPG?raw=true)

## Connecting Android Devices ##

You can connect any android devices via ADB from within the guest OS. To connect to an emulator, run command `adb connect 10.0.2.2:<adb_port>`. The adb port for all newer emulators (QEMU2) are the emulator's console port + 1. For example, if you have 'emulator-5554' running, your adb port would be 5555. Running `adb connect 10.0.2.2:5555` will connect to your host's emulator. All Samsung hardware devices are also supported via USB. Simply plugin your phone (debugging enabled), `adb kill-server`, `adb start-server`, and you should see the authorization prompt on your phone. Accept the fingerprint and you're good to go!

## Multi Monitor Setups ##

1. Enable displays via VirtualBox.

![Add Display](/resources/enableDisplay.JPG?raw=true)

2. Configure display setup via Display settings.

![Display Settings](/resources/displaySettings.JPG?raw=true)

![Config Display](/resources/configDisplay.JPG?raw=true)


# Start developing! 
