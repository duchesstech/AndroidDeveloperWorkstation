# Encoding: UTF-8
# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'

props = YAML.load_file 'vagrant_properties.yml'

#Loading in log.rb for redirecting stdout/stderr to log file.
load 'scripts/log.rb'

#Locale/Language Encodings
ENV["LC_ALL"] = props['system']['locale_setting']
ENV["LC_CTYPE"] = props['system']['locale_setting']
ENV["LANGUAGE"] = props['system']['locale_setting']
ENV["LANG"] = props['system']['locale_setting']

Vagrant.configure('2') do |config|

    #Vagrant box specs
    config.vm.box = props['box']['base_box']
    config.vm.box_url = props['box']['box_url']
    config.vm.box_check_update = false
    config.ssh.username = "vagrant"
    config.ssh.forward_agent = true
    
    #Virtualbox provider specific configuration
    config.vm.provider 'virtualbox' do |vb|
        vb.name = props['virtualbox']['machine_name']
        vb.gui = true
        vb.memory = props['virtualbox']['virtualbox_memory']
        vb.customize ['modifyvm', :id, '--monitorcount', props['virtualbox']['monitor_count']]
        vb.customize ['modifyvm', :id, '--vram', props['virtualbox']['vram']]
        vb.customize ['modifyvm', :id, '--audio', 'dsound', '--audiocontroller', 'ac97']
        vb.customize ['modifyvm', :id, '--usb', 'on']
        #Note, need Virtualbox Extension Pack for USB 2.0/3.0
        vb.customize ['modifyvm', :id, '--usbehci', 'on']
        
        vb.customize ['usbfilter', 'add', '0', '--target', :id, '--name', 'Samsung Device', '--vendorid', '0x04e8']
        
        #Add another disk
        #file_to_disk = './tmp/large_disk.vdi'
        #unless File.exist?(file_to_disk)
        #    config.vm.customize ['createhd', '--filename', file_to_disk, '--size', 30 * 1024] #30GB
        #end
        #config.vm.customize ['storageattach', :id, '--storagectl', 'SATA Controller', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', file_to_disk]
    end
    
    #Basic machine configuration
    config.vm.provision "init_machine_shell", type: "shell", privileged: false, path: "scripts/init_machine.sh"
    
    #Install Java7
    java7_install_dir = "/opt/jdk_#{props['java']['java7_version_alt']}"
    java7_tarzip = "jdk-#{props['java']['java7_version_alt']}-linux-x64.tar.gz"
    config.vm.provision "java_7", type: "shell", privileged: false do |java7|
        java7.env = {java_tarzip:java7_tarzip, java_download_url:props['java']['java7_download_url'], java_install_dir:java7_install_dir, java_version:props['java']['java7_version'], makeHome:'false'}
        java7.path = 'scripts/java_install.sh'
    end
    
    #Install Java8
    java8_install_dir = "/opt/jdk_#{props['java']['java8_version_alt']}"
    java8_tarzip = "jdk-#{props['java']['java8_version_alt']}-linux-x64.tar.gz"
    config.vm.provision "java_8", type: "shell", privileged: false do |java8|
        java8.env = {java_tarzip:java8_tarzip, java_download_url:props['java']['java8_download_url'], java_install_dir:java8_install_dir, java_version:props['java']['java8_version'], makeHome:'true'}
        java8.path = 'scripts/java_install.sh'
    end
    
    #Install Git
    config.vm.provision "git", type: "shell", privileged: false do |git|
        git.env = {user_name:props['git']['username'], user_email:props['git']['email']}
        git.path = 'scripts/git_install.sh'
    end
    
    #Install SDKMAN (helps with any installations Java related, JAVA_HOME must be defined prior to install)
    config.vm.provision "sdkman", type: "shell", privileged: false, inline: <<-SHELL
        curl -s get.sdkman.io | bash
    SHELL
    
    #Install Gradle
    config.vm.provision "gradle", type: "shell", privileged: false do |gradle|
        gradle.env = {gradle_version:props['gradle']['version']}
        gradle.path = 'scripts/gradle_install.sh'
    end
    
    #Install Ant
    ant_tarzip = "apache-ant-#{props['ant']['version']}-bin.tar.gz"
    config.vm.provision "ant", type: "shell", privileged: false do |ant|
        ant.env = {install_dir:'/opt/ant', version:props['ant']['version']}
        ant.path = 'scripts/ant_install.sh'
    end
    
    #Install AndroidSDK
    config.vm.provision "android_sdk", type: "shell", privileged: false do |androidsdk|
        androidsdk.env = {install_dir:'/home/vagrant/Android/Sdk', packages:props['androidsdk']['packages'], version: props['androidsdk']['version']}
        androidsdk.path = 'scripts/android_sdk_install.sh'
    end
    
    #Install AndroidStudio
    config.vm.provision "android_studio", type: "shell", privileged: false do |androidstudio|
        androidstudio.env = {download_url:props['androidstudio']['download_url'], zip_name:props['androidstudio']['zip_name']}
        androidstudio.path = 'scripts/android_studio_install.sh'
    end
    
    #Install Node.js
    config.vm.provision "node_js", type: "shell", privileged: false do |nodejs|
        nodejs.env = {nvm_version:props['nodejs']['nvm_version'], node_version:props['nodejs']['node_version']}
        nodejs.path = 'scripts/nodejs_install.sh'
    end
    
    #Install Appium (Node.js should be installed first)
    config.vm.provision "appium", type: "shell", privileged: false do |appium|
        appium.env = {version:props['appium']['version']}
        appium.path = 'scripts/appium_install.sh'
    end
    
    config.vm.provision "end_message", type: "shell", privileged: false, inline: <<-SHELL
        echo "##############################################################################################"
        echo "Provisioning complete. Please log into your VirtualBox machine and run command 'startxfce4&'"
        echo "##############################################################################################"
    SHELL
end