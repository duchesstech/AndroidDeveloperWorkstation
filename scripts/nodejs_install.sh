#!/bin/bash
sudo apt-get install -y build-essential libssl-dev
curl -sL "https://raw.githubusercontent.com/creationix/nvm/v${nvm_version}/install.sh" -o install_nvm.sh
sudo chmod +x install_nvm.sh
./install_nvm.sh
rm install_nvm.sh
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"
nvm install "${node_version}"
nvm use "${node_version}"
