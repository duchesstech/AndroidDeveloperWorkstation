#!/bin/bash

if [[ -f '/vagrant/shared/github_rsa' ]]; then
    echo 'GitHub SSH Key copying over to VM.'
    cp /vagrant/shared/github_rsa ~/.ssh/
    chmod 600 ~/.ssh/github_rsa
    touch ~/.ssh/config
    echo "IdentityFile ~/.ssh/github_rsa" >> ~/.ssh/config
else
    echo 'GitHub SSH Key does not exist. Not copying key over to VM.'
fi

sudo apt-get -y install git

if [[ -z "${user_name}" || -z "${user_email}" ]]; then
    echo 'Git username/email not configured.'
else
    echo "Configuring git username: ${user_name} and email: ${user_email}"
    git config --global user.name "${user_name}"
    git config --global user.email "${user_email}"
fi