#!/bin/bash
sudo mkdir ${java_install_dir}
sudo wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" "${java_download_url}" -P "${java_install_dir}"
sudo tar -zxf "${java_install_dir}/${java_tarzip}" -C "${java_install_dir}"
sudo rm "${java_install_dir}/${java_tarzip}"

if [ "${makeHome}" = "true" ]; then
    #Note that ${java_install_dir} must be an absolute path (prefixed with a backslash '/')
    sudo update-alternatives --install "/usr/bin/java" java "${java_install_dir}/jdk${java_version}/bin/java" 100
    sudo update-alternatives --install "/usr/bin/javac" javac "${java_install_dir}/jdk${java_version}/bin/javac" 100
    sudo update-alternatives --install "/usr/bin/javaws" javaws "${java_install_dir}/jdk${java_version}/bin/javaws" 100
    . /vagrant/scripts/set_env_var.sh "JAVA_HOME" "${java_install_dir}/jdk${java_version}" false
fi
