#!/bin/bash
if [ "${3}" = "true" ]; then
    echo "export ${1}=\$${1}:${2}" >> ~/.bashrc
else
    echo "export ${1}=${2}" >> ~/.bashrc
fi