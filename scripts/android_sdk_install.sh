#!/bin/bash

#See: https://code.google.com/p/android/issues/detail?id=82711
sudo apt-get -y install lib32z1 lib32ncurses5 lib32bz2-1.0 lib32stdc++6

mkdir -p ${install_dir}
wget "http://dl.google.com/android/android-sdk_r${version}-linux.tgz" -P ${install_dir}
tar -zxf "${install_dir}/android-sdk_r${version}-linux.tgz" -C ${install_dir} --strip-components 1
. /vagrant/scripts/set_env_var.sh "ANDROID_HOME" "${install_dir}" false
. /vagrant/scripts/set_env_var.sh "PATH" "\$ANDROID_HOME/tools" true
. /vagrant/scripts/set_env_var.sh "PATH" "\$ANDROID_HOME/platform-tools" true

export ANDROID_HOME=${install_dir}
export PATH=$ANDROID_HOME/platform-tools:$PATH
export PATH=$ANDROID_HOME/tools:$PATH

#Install Android Packages
( sleep 5 && while [ 1 ]; do sleep 1; echo y; done ) | "${install_dir}/tools/android" update sdk --no-ui --all --filter ${packages}

#Plugdev group permissions
sudo adduser vagrant plugdev
sudo adduser root plugdev

#See https://developer.android.com/studio/run/device.html
sudo cp /vagrant/shared/android.rules /etc/udev/rules.d/51-android.rules
sudo chown root:plugdev /etc/udev/rules.d/51-android.rules
sudo chmod g+w /etc/udev/rules.d/51-android.rules
sudo service udev restart
sudo kill all adb
sudo rm -R ~/.android
adb start-server
