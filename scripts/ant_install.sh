#!/bin/bash
sudo mkdir ${install_dir}
sudo wget "http://archive.apache.org/dist/ant/binaries/apache-ant-${version}-bin.tar.gz" -P ${install_dir}
sudo tar -zxf "${install_dir}/apache-ant-${version}-bin.tar.gz" -C ${install_dir}
. /vagrant/scripts/set_env_var.sh "ANT_HOME" "${install_dir}/apache-ant-${version}" false
. /vagrant/scripts/set_env_var.sh "PATH" "\$ANT_HOME/bin" true
sudo rm "${install_dir}/apache-ant-${version}-bin.tar.gz"