#!/bin/bash
sudo apt-get update
sudo /usr/share/debconf/fix_db.pl
sudo apt-get install dictionaries-common
sudo apt-get -y upgrade
        
#XFCE
sudo apt-get install -y xfce4 virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11
sudo apt-get install gnome-icon-theme-full tango-icon-theme
sudo sh -c "echo allowed_users=anybody > /etc/X11/Xwrapper.config"
        
#Zip/Unzip
sudo apt-get -y install zip
sudo apt-get -y install unzip

#Chromium
sudo apt-get install -y chromium-browser
        
#gedit
sudo apt-get install -y gedit